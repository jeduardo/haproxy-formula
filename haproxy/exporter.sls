# vim: tw=135
{% from "haproxy/map.jinja" import haproxy with context %}

haproxy_exporter_binary:
  archive.extracted:
    - name: {{ haproxy.exporter_dist_dir }}
    - source: https://github.com/prometheus/haproxy_exporter/releases/download/v{{ haproxy.exporter_version }}/haproxy_exporter-{{ haproxy.exporter_version }}.linux-amd64.tar.gz
    - source_hash: {{ haproxy.exporter_archive_hash }}
    - keep: True

haproxy_exporter_create_symlink:
  file.symlink:
    - name: /usr/local/bin/haproxy-exporter
    - target: {{ haproxy.exporter_dist_dir }}/haproxy_exporter-{{ haproxy.exporter_version }}.linux-amd64/haproxy_exporter

haproxy_exporter_initscript:
  file.managed:
{% if salt['grains.get']('init') == 'systemd' %}
    - name: /etc/systemd/system/haproxy-exporter.service
    - contents: |
        [Unit]
        After=network-online.target
        Requires=network-online.target

        [Service]
        Restart=on-failure
        ExecStart=/usr/local/bin/haproxy-exporter \
          --haproxy.scrape-uri={{ haproxy.exporter_socket_path }}

        [Install]
        WantedBy=multi-user.target
{% else %}
    - name: /etc/init.d/haproxy-exporter
    - source: salt://haproxy/templates/exporter_initscript.sh
    - mode: 755
    - user: root
    - group: root
    - template: jinja
    - context:
        haproxy: {{ haproxy|json}}
{% endif %}
haproxy-exporter:
  service.running:
    - enable: true
    - require:
      - file: haproxy_exporter_initscript
    - watch:
      - file: haproxy_exporter_initscript

{% if salt['grains.get']('init') != 'systemd' %}
# Salt service module can apparently not start a sysV init service
# so we manually do that
manually-start-exporter:
  cmd.run:
    - name: service haproxy-exporter restart
    - onchanges:
      - file: /etc/init.d/haproxy-exporter
{% endif %}
